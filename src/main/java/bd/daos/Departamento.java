package bd.daos;

import bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raphael Bragança
 */
public class Departamento {

    public static ArrayList<departamento.Departamento> buscar() {
        ArrayList<departamento.Departamento> result = new ArrayList<>();
        Conexao con = Conexao.getInstancia();
        String query = "SELECT * "
                + "FROM departamento ORDER BY id";
        con.prepararAI(query);
        ResultSet rs = con.executeQuery();
        try {
            while (rs.next()) {
                result.add(new departamento.Departamento(rs.getInt("id"), rs.getInt("idPai"),
                        rs.getInt("idDepartamento"), rs.getString("descricao")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Departamento.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return result;
        }

    }

    public static departamento.Departamento buscar(int id) {
        departamento.Departamento result = null;
        Conexao con = Conexao.getInstancia();
        String query = "SELECT idPai "
                + "FROM departamento WHERE id=?";
        con.prepararAI(query);
        System.out.println(query);

        try {
            con.getPs().setInt(1, id);
            ResultSet rs = con.executeQuery();
            if (rs.next()) {
                result = new departamento.Departamento(id, rs.getInt("idPai"),
                        rs.getInt("idDepartamento"), rs.getString("descricao"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Departamento.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return (result);
        }
    }

    public static boolean inserir(departamento.Departamento departamento) {
        boolean result = false;
        Conexao con = Conexao.getInstancia();
        String query = "INSERT INTO departamento (id, idPai, idUnidade, descricao) VALUES (?,?,?,?)";
        con.prepararAI(query);

        try {

            con.getPs().setInt(1, departamento.getId());
            con.getPs().setInt(2, departamento.getIdPai());
            con.getPs().setInt(3, departamento.getIdUnidade());
            con.getPs().setString(4, departamento.getDescricao());
            result = con.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Departamento.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return (result);
        }
    }

    public static boolean excluir(int id) {
        boolean result = false;
        Conexao con = Conexao.getInstancia();
        String query = "DELETE FROM departamento WHERE id=?";
        con.prepararAI(query);
        try {
            con.getPs().setInt(1, id);
            result = con.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Departamento.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return (result);
        }
    }
}
