package bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {

    private static Conexao instancia = null;
    private Connection con = null;
    private PreparedStatement ps = null;

    public PreparedStatement getPs() {
        return ps;
    }

    private Conexao() {
        
        String server = "localhost";
        String database = "ramais";
        String usuario = "root";
        String senha = "root";
        

        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.con = DriverManager.getConnection("jdbc:mysql://" + server
                    + "/" + database ,usuario, senha);
 

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Conexao getInstancia() {
        try {
            if (instancia == null || !instancia.con.isValid(0)) {
                instancia = new Conexao();
            }
        } catch (SQLException ex) {
            instancia = new Conexao();
        }
        finally {
            return(instancia);
        }
        
    }

    public void preparar(String sql) {
        try {
            this.ps = this.con.prepareStatement(sql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void prepararAI(String sql) {
        try {
            this.ps = this.con.prepareStatement(sql,
                    Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getAutoIncrement() {
        try {
            ResultSet rs = this.ps.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return (0);
    }

    public void fechar() {
        try {
            this.ps.close();
            this.con.close();
            this.instancia = null;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public ResultSet executeQuery() {
        try {
            return (this.ps.executeQuery());
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public boolean executeUpdate() {
        try {
            return (this.ps.executeUpdate() > 0);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return (false);
    }

    public void iniciaTransacao() {
        try {
            this.con.setAutoCommit(false);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void fecharTransacao(boolean b) {
        try {
            if (b) {
                this.con.commit();
            } else {
                this.con.rollback();
            }
            this.con.setAutoCommit(true);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
