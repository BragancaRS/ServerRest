package departamento;

/**
 *
 * @author raphael
 */
public class Departamento {

    private int id;
    private int idPai;
    private int idUnidade;
    private String descricao;

    public Departamento(int id, int idPai, int idUnidade, String descricao) {
        this.id = id;
        this.idPai = idPai;
        this.idUnidade = idUnidade;
        this.descricao = descricao;
    }

    public Departamento() {
        this.id = 0;
        this.idPai = 0;
        this.idUnidade = 0;
        this.descricao = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPai() {
        return idPai;
    }

    public void setIdPai(int idPai) {
        this.idPai = idPai;
    }

    public int getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(int idUnidade) {
        this.idUnidade = idUnidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
