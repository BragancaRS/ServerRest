package br.unitins.serverrest.services.rest;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import departamento.Departamento;

/**
 *
 * @author fredsonvieiracosta
 */
@Path("/departamento")
public class ServDepartamento {

    @GET
    @Path("/buscar")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDepartamentos() {
        Gson gson = new Gson();
        String result = gson.toJson(bd.daos.Departamento.buscar(),
                ArrayList.class);
        return (result);
    }

    @GET
    @Path("/buscar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDepartamentos(@PathParam("id") int id) {
        Gson gson = new Gson();
        String result = gson.toJson(bd.daos.Departamento.buscar(id),
                departamento.Departamento.class);
        return (result);
    }

    @POST
    @Path("/inserir")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void postDepartamento(@FormParam("descricao") String descricao, @FormParam("idPai") int idPai,
            @FormParam("idUnidade") int idUnidade,
            @Context HttpServletResponse response) {
        boolean result = bd.daos.Departamento.inserir(
                new departamento.Departamento(0, idPai, idUnidade, descricao));
        try {
            if (result) {
                response.sendRedirect("http://localhost:8080/restclienthtml/index.jsp");
            } else {
                response.sendRedirect("http://www.uol.com.br");
            }
        } catch (IOException ex) {
            Logger.getLogger(Departamento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @DELETE
    @Path("/excluir/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteDepartamento(@PathParam("id") int id) {
        return (new Gson().toJson(bd.daos.Departamento.excluir(id)));
    }

    @PUT
    @Path("/editar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String putDepartamento(@PathParam("id") int id, Departamento unidade) {
        Gson gson = new Gson();
        boolean result = bd.daos.Departamento.excluir(id);
        return (gson.toJson(result));
    }
}
